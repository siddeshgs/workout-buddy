import React ,{ useEffect, useState } from 'react'
import WorkoutDetails from "../component/workoutDetails";
import WorkoutForm from '../component/WorkoutForm';
import { useWorkoutsContext } from '../hooks/useWorkoutsContext'


const Home = () =>{

    // const [workouts, setWorkouts] = useState(null)
    const { workouts, dispatch } = useWorkoutsContext()
    console.log(workouts,"workou")

    const fetchWorkouts = async () => {
        const res = await fetch('/api/workouts')
        const json = await res.json();

        if(res.ok){
            // setWorkouts(json)
            dispatch({
                type: 'SET_WORKOUTS',
                payload: json,
            })
        }
      }
    
    useEffect(() => {
      fetchWorkouts()
    }, [])
    
    return (
        <div className="home">
            {/* <h3 className="work-heading">
                Total Workouts
            </h3> */}
            <div className="work">
                {workouts && workouts.map((workout) => (
                    <WorkoutDetails key={workout._id} workout={workout} />
                ))}
            </div>
            <WorkoutForm />
        </div>
    )
}

export default Home