import React, { useState } from 'react'
import { useWorkoutsContext } from "../hooks/useWorkoutsContext"

function WorkoutForm({fetchWorkouts}) {

    const { workouts, dispatch } = useWorkoutsContext()
    const [title,setTitle] = useState('')
    const [load,setLoad] = useState('')
    const [reps,setReps] = useState('')
    const [error, setError] = useState(null);

    const handleSubmit = async (e)=>{
        e.preventDefault();

        const workout = { title, load, reps };

        const response = await fetch('/api/workouts',{
            method: 'POST',
            body: JSON.stringify(workout),
            headers: {
                'Content-type': "application/json"
            }
        })
        const json = await response.json()

        if(!response.ok){
            setError(json.error)
        }else{
            setError(null)
            console.log('added new workout form: ', json)
            setTitle('')
            setLoad('')
            setReps('')
            dispatch({
                type: 'CREATE_WORKOUT',
                payload: json
            })
        }
    }

  return (
    <form className="create-form" onSubmit={handleSubmit}>
        <h3>Add a new Workouts</h3>
        
        <div className="form-item">
            <label>Title:</label>
            <input type='text' onChange={(e)=>setTitle(e.target.value)} value={title}/>
        </div>

        <div className="form-item">
            <label>Load (kg):</label>
            <input type='number' onChange={(e)=>setLoad(e.target.value)} value={load}/>
        </div>

        <div className="form-item">
            <label>Reps :</label>
            <input type='number' onChange={(e)=>setReps(e.target.value)} value={reps}/>
        </div>

        <button>Add workout</button>
        {error && <div className="error">{error}</div> }
    </form>
  )
}

export default WorkoutForm