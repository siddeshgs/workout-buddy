import React from 'react'
import { useWorkoutsContext } from '../hooks/useWorkoutsContext';

function WorkoutDetails({ workout }) {

    const { workouts, dispatch } = useWorkoutsContext()

    const deleteWorkout = async (id) => {
        const res = await fetch(`/api/workouts/${id}`, {
            method: 'DELETE',
            
        });
        const json = await res.json()

        if(res.ok){
            dispatch({
                type: 'DELETE_WORKOUT', payload: json
            })
        }
    }

  return (
    <div className="workout-details">
        <h4>{workout.title}</h4>
        <p><strong>Load (kg): </strong>{workout.load}</p>
        <p><strong>Reps:</strong>{workout.reps}</p>
        <p>{workout.createdAt}</p>
        <div className='delete-icon' onClick={() => deleteWorkout(workout._id)}>X</div>
    </div>
  )
}

export default WorkoutDetails