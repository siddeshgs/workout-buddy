import { BrowserRouter , Routes, Route} from "react-router-dom"
import Home from "./pages/home";
import Navbar from "./component/Navbar";
import './App.css'

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Navbar/>
        <Routes>
          <Route path="/" element={<Home />}/>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
