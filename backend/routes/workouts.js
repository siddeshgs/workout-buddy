const express = require('express');
const workoutSchema = require('../models/workout')
const {
    getAllWorkout,
    addNewWorkout,
    getSingleWorkout,
    deleteWorkout,
    updateWorkout
} = require('../controllers/workoutController')

const router = express.Router();

// GET all workouts
router.get('/', getAllWorkout)

// GET a single workout
router.get('/:id', getSingleWorkout)

// POST a new workout
router.post('/', addNewWorkout )

// DELETE a workout
router.delete('/:id', deleteWorkout)

// UPDATE a workout
router.put('/:id', updateWorkout)

module.exports = router