const workoutSchema = require('../models/workout')
const mongoose = require('mongoose')
const getAllWorkout = async (req, res) => {
    try {
        const workouts = await workoutSchema.find({}).sort({ createdAt : -1})
        res.status(200).json(workouts)
    } catch (error) {
        res.status(400).json({error: error.message})
    }
}

const addNewWorkout = async (req, res) => {
    console.log(req.body)
    const {title, load, reps} = req.body;
    try {
       const workout = await workoutSchema.create({title, load, reps})
        res.status(200).json(workout)
    } catch (error) {
        res.status(400).json({error: error.message})
    }
}

const getSingleWorkout = async (req, res) => {
    try{
        const {id} = req.params;
        if(!mongoose.Types.ObjectId.isValid()){
            res.status(404).json({error: " no such workouts"})
        }
        const workout = await workoutSchema.findById(id)
        res.status(200).json({workout})
    }catch(error){
        res.status(400).json({error: error.message})
    }
}

const deleteWorkout = async (req, res) => {
    try{
        const { id } = req.params;

        if(!mongoose.Types.ObjectId.isValid){
            return res.status(404).json({ error: "No such workouts found to delete"})
        }

        const workout = await workoutSchema.findByIdAndDelete(id)

        res.status(200).json(workout)
    }catch{
        res.status(400).json({error: error.message})
    }
}

const updateWorkout = async (req, res) => {
    try{
        const { id } = req.params;
        const { title, load, reps } = req.body;

        if( !mongoose.Types.ObjectId.isValid ){
            return res.status(404).json({ error: "No such workouts found to update"})
        }

        const workout = await workoutSchema.findOneAndUpdate({_id: id},{ title, load, reps })

        if(!workout){
            return res.status(404).json({ error: "No sch workout found to update"})
        }

        res.status(200).json(workout)
    }catch{
        res.status(400).json({error: error.message})
    }
}

module.exports = {
    getAllWorkout,
    getSingleWorkout,
    addNewWorkout,
    deleteWorkout,
    updateWorkout
}