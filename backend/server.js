require('dotenv').config()
const express = require('express')
const mongoose = require('mongoose')
const body_parser = require('body-parser')
const workoutRoutes = require('./routes/workouts')

// express app
const app = express()
const PORT = process.env.PORT;
const MONGO_URI = process.env.MONGO_URI;

// middleware
app.use(body_parser.json())

app.use((req,res,next)=>{
    console.log(req.path,req.method, req.body)
    // res.header('Access-Control-Allow-Origin', '*');
    next()
})

// routes
app.use('/api/workouts', workoutRoutes)

// connect to mongo db
mongoose.connect(MONGO_URI)
    .then(()=>{
        // listen for requests
        app.listen(PORT, ()=>{
            console.log('connected to db & listening on port '+ PORT)
        })
    })
    .catch((err)=>{
        console.log(err)
    })


